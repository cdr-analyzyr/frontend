import Vue from 'vue'
import App from './App.vue'
import socketio from 'socket.io-client';
import VueSocketIO from 'vue-socket.io';
import VueGoodTable from 'vue-good-table';
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'
import VueRouter from 'vue-router'

console.log(process.env.NODE_ENV);
export const SocketInstance = socketio('http://192.168.22.70:33312');

Vue.use(VueMoment, {
  moment,
})
Vue.use(VueRouter);
Vue.use(VueGoodTable);
Vue.use(VueSocketIO, SocketInstance);

const routes = [
  {path: "/", component: App}

]

const router = new VueRouter({
  routes
})

new Vue({
  el: '#App',
  router
})
